#!/usr/bin/python
import smbus
import time
import math
import sys
import spidev
import time
import sys
#######################################################################################
#Temperaturkorrigierte Elektrodensteilheit mV pro pH
def phF1(temp):
  return  0.1984 * temp + 54.2
#Ph7 Offset in uV
phF2 = -54176
#Ph4 Scaling (skaliert Elektrodensteilheit und Temperaturkoeffizient
phF3 = 1.16




#######################################################################################
# Convert Ohm to Grad Temperature of NTC
def tempF1(x):
  return (-8.951267053 * math.log(x - 1165.055465) + 86.80088379)



# Temperatur an der der EC Skalierungfaktor bestimmt wurde in Grad
ecF1 = 20
# Temperaturkoeffiziert der Loesung in unc_mS pro grad
#ecF2 = .03028192257
#ecF2 = 0.026775220
#ecF2 = 0.03018 # unc_mS pro grad
ecF2 = 0.0204# perc pro grad -> 
# Skalierungsfaktor
#ecF3 = 0.87990120407533
ecF3 = 0.951

# I2C-Adresse des MCP23017
SWITCH_ADDR = 0x48
ADC_ADDR = 0xD

REG_CONTROL_16 = 0x80
REG_START_FRQ_24 = 0x82
REG_FRQ_INC_24 = 0x85
REG_NUM_INC_16 = 0x88
REG_SETTLE_TIME_16 = 0x8A
REG_STATUS = 0x8F
REG_REAL_16 = 0x94
REG_IMAGINARY_16 = 0x96

CONTROL_NOP1 = 0x0
CONTROL_INIT_START_FRQ = 0x1
CONTROL_START_SWEEP = 0x2
CONTROL_INC_FRQ = 0x3
CONTROL_REP_FRQ = 0x4
CONTROL_NOP2 = 0x8
CONTROL_NOP3 = 0x9
CONTROL_POWERDOWN = 0xA
CONTROL_STANDBY = 0xB
CONTROL_NOP4 = 0xC
CONTROL_NOP5 = 0xD

RANGE_2V = 0
RANGE_200MV = 1
RANGE_400MV = 2
RANGE_1V = 3
RANGE = RANGE_1V

PGA_GAIN_1X = 1
PGA_GAIN_5X = 0

RFB_100 = 0x1
RFB_1000 = 0x2

def word_to_array(word):
  return [(word & 0xff)]


def word_16_to_array(word):
  return [(word >> 8) & 0xff, word & 0xff]

def word_24_to_array(word):
  return [(word >> 16) & 0xff, (word >> 8) & 0xff, word & 0xff]

def array_to_word(array):
  return (array[0])

def array_to_word_16(array):
  return (array[0] << 8) | (array[1])

def array_to_word_24(array):
  return (array[0] << 16) | (array[1] << 8) | (array[2])

def set_switch(val):
  i2c.write_byte(SWITCH_ADDR,val)

def set_switch_y(rfb):
  set_switch(0x80 | rfb)

def set_switch_rtd(rfb):
  set_switch(0x40 | rfb)

def set_switch_10000(rfb):
  set_switch(0x20 | rfb)

def set_switch_1000(rfb):
  set_switch(0x10 | rfb)

def set_switch_100(rfb):
  set_switch(0x8 | rfb)
  
def read_reg(reg):
  a = [i2c.read_byte_data(ADC_ADDR, reg+0)]
  return array_to_word(a)

def write_reg(reg, val):
  a = word_to_array(val)
  i2c.write_byte_data(ADC_ADDR, reg+0, a[0])
  
def read_reg_16(reg):
  a = [i2c.read_byte_data(ADC_ADDR, reg+0),
       i2c.read_byte_data(ADC_ADDR, reg+1)]
  return array_to_word_16(a)

def write_reg_16(reg, val):
  a = word_16_to_array(val)
  i2c.write_byte_data(ADC_ADDR, reg+0, a[0])
  i2c.write_byte_data(ADC_ADDR, reg+1, a[1])
  
def read_reg_24(reg):
  a = [i2c.read_byte_data(ADC_ADDR, reg+0),
       i2c.read_byte_data(ADC_ADDR, reg+1),
       i2c.read_byte_data(ADC_ADDR, reg+2)]
  return array_to_word_24(a)

def write_reg_24(reg, val):
  a = word_24_to_array(val)
  i2c.write_byte_data(ADC_ADDR, reg+0, a[0])
  i2c.write_byte_data(ADC_ADDR, reg+1, a[1])
  i2c.write_byte_data(ADC_ADDR, reg+2, a[2])

def set_start_frq(frq):
  write_reg_24(REG_START_FRQ_24, frq)
  if (read_reg_24(REG_START_FRQ_24) != frq):
    print "Error in set_start_frq"
    return False
  return True
  
def set_frq_inc(frq):
  write_reg_24(REG_FRQ_INC_24, frq)
  if (read_reg_24(REG_FRQ_INC_24) != frq):
    print "Error in set_frq_inc"    
    return False
  return True

def set_num_inc(num):
  write_reg_16(REG_NUM_INC_16, num)
  if (read_reg_16(REG_NUM_INC_16) != num):
    print "Error in set_inc_num"    
    return False
  return True

def set_control(mode,range_v,gain):
#INTERNAL CLOCK
  v = (mode << 12) | (range_v << 9) | (gain << 8)
  write_reg_16(REG_CONTROL_16, v)
  if (read_reg_16(REG_CONTROL_16) != v):
    print "Error in set_control"    
    return False
  return True

def set_settling_time(time):
# CYCLES x1
  v = (0x0 << 9) | (time & 0x1FF)
  write_reg_16(REG_SETTLE_TIME_16, v)
  if (read_reg_16(REG_SETTLE_TIME_16) != v):
    print "Error in set_settling_time"    
    return False
  return True

def status_is_data_valid():
  v = read_reg(REG_STATUS)
  if (v & 0x2):
    return True
  return False

def status_is_sweep_done():
  v = read_reg(REG_STATUS)
  if (v & 0x4):
    return True
  return False

def read_real():
  return read_reg_16(REG_REAL_16)

def read_imaginary():
  return read_reg_16(REG_IMAGINARY_16)

def read_mag():
  r = read_real()
  i = read_imaginary()
  if r >= 1<<15: r -= 1<<16
  if i >= 1<<15: i -= 1<<16    
  return math.sqrt(r*r+i*i)

def measure(range_v, gain, rfb):
  set_switch_y(rfb)

  set_control(CONTROL_STANDBY, range_v, gain)
  set_control(CONTROL_INIT_START_FRQ, range_v, gain)
  set_control(CONTROL_START_SWEEP, range_v, gain)
  
  while True:
    if status_is_data_valid():
      break
  return read_mag()

def measure_temp(range_v, gain, rfb):
  set_switch_rtd(rfb)

  set_control(CONTROL_STANDBY, range_v, gain)
  set_control(CONTROL_INIT_START_FRQ, range_v, gain)
  set_control(CONTROL_START_SWEEP, range_v, gain)
  
  while True:
    if status_is_data_valid():
      break
  return read_mag()


def calib(range_v, gain, rfb):

  # Set Calib resistor and define expected y

#  set_switch_1000()
#  yl = 0.001000
#  yl = 0.000875  

  set_switch_10000(rfb)
  yl = 0.0001

  set_control(CONTROL_STANDBY, range_v, gain)
  set_control(CONTROL_INIT_START_FRQ, range_v, gain)
  set_control(CONTROL_START_SWEEP, range_v, gain)
  
  while True:
    if status_is_data_valid():
      break    
  nl = read_mag()

# Set Calib resistor and define expected y  
#  set_switch_100()
#  yh = 0.01
  set_switch_1000(rfb)
  yh = 0.001

  set_control(CONTROL_STANDBY, range_v, gain)
  set_control(CONTROL_INIT_START_FRQ, range_v, gain)
  set_control(CONTROL_START_SWEEP, range_v, gain)

  while True:
    if status_is_data_valid():
      break
  nh = read_mag()
  
  gf = (yh-yl)/(nh-nl)  
  nos = nh-(yh/gf)
  
  return (gf, nos)

def calib_temp(range_v, gain, rfb):

  # Set Calib resistor and define expected y

#  set_switch_1000()
#  yl = 0.001000
#  yl = 0.000875  

  set_switch_10000(rfb)
  yl = 0.0001

  set_control(CONTROL_STANDBY, range_v, gain)
  set_control(CONTROL_INIT_START_FRQ, range_v, gain)
  set_control(CONTROL_START_SWEEP, range_v, gain)
  
  while True:
    if status_is_data_valid():
      break    
  nl = read_mag()

# Set Calib resistor and define expected y  
#  set_switch_100()
#  yh = 0.01
  set_switch_1000(rfb)
  yh = 0.001

  set_control(CONTROL_STANDBY, range_v, gain)
  set_control(CONTROL_INIT_START_FRQ, range_v, gain)
  set_control(CONTROL_START_SWEEP, range_v, gain)

  while True:
    if status_is_data_valid():
      break
  nh = read_mag()
  
  gf = (yh-yl)/(nh-nl)  
  nos = nh-(yh/gf)
  
  return (gf, nos)

#################
#################
#################

i2c = smbus.SMBus(1)

set_start_frq(0x418937)
set_frq_inc(0x418937/16)
set_num_inc(1)
set_settling_time(1)

range_v = RANGE_2V
gain = PGA_GAIN_5X
rfb = RFB_1000

(gf, nos) = calib_temp(range_v, gain, rfb)
t = 0
avg = 5
for i in range(avg):
  val = measure_temp(range_v, gain, rfb)
  t = t + int((1/((val-nos)*gf)))
temp = t / avg  
print str(temp) + " " + "Ohm "
temp = tempF1(temp)
print str(temp) + " C "
#iface.dbi_set("nt", int(trans(temp)))

range_v = RANGE_200MV
gain = PGA_GAIN_5X
rfb = RFB_1000

(gf, nos) = calib(range_v, gain, rfb)
t = 0
avg = 10
for i in range(avg):
  val = measure(range_v, gain, rfb)
  t = t + int((val-nos)*gf*1000000)/1000.0
t = t /avg
print str(t) + " " + "unc"
t = (t *(1 + ((ecF1-temp)*ecF2))) * ecF3
# 230 calib temp
# 0.916 total scal without temp influence (at
#2.651946328
print str(t) + " " + "mSuc"
#iface.dbi_set("nc", int(t*100))

set_control(CONTROL_POWERDOWN, RANGE_200MV, PGA_GAIN_1X)











##########################################
##########################################

REG_COMM_W = 0x0
REG_STATUS_R = 0x0
REG_MODE_16 = 0x1
REG_CONF_16 = 0x2
REG_DATA_24 = 0x3
REG_ID = 0x4
REG_IO = 0x5
REG_OFFSET_24 = 0x6
REG_FULL_24 = 0x7

MODE_CONTINUOUS = 0x0
MODE_SINGLE = 0x1
MODE_IDLE = 0x2
MODE_POWERDOWN = 0x3
MODE_INTERNAL_ZERO = 0x4
MODE_INTERNAL_FULL = 0x5
MODE_SYSTEM_ZERO = 0x6
MODE_SYSTEM_FULL = 0x7

# 500ms conv cycle
UPDATE_RATE = 0xf
# Internal clock and output on CLK pin
CLOCK_REF = 0x0

# Internal or external reference
REF_SEL = 0
# Buffer adc input
BUFFER_INPUT = 0
# Gain settings
GAIN = 0
# boosting
BOOST = 0
# AIN1- is biased
BIAS = 0
# POLARITY
POLARITY = 0

### IO
IEXCDIR = 0x1
IEXCEN = 0x2

def comm_reg(rnw, reg):
    v = 0
    
    if (rnw):
        v = v | 0x40
    if ((reg >= 0) and (reg <= 7)):
        v = v | (reg << 3)
    return v
def status_is_err(reg):
    if (reg & 0x40):
        return True
    return False
def status_is_ready(reg):
    if (reg & 0x80):
        return False    
    return True
def status_get_ch(reg):
    if (reg & 0x1):
        return 0x0
    if (reg & 0x2):
        return 0x1
    if (reg & 0x4):
        return 0x2
    return None


def read_reg(reg):
    read = spi.xfer2([comm_reg(1,reg), 0xff])
    return (read[1])
def write_reg(reg, val):
    spi.xfer2([comm_reg(0,reg), (val & 0xff)])
def read_16bit_reg(reg):
    read = spi.xfer2([comm_reg(1,reg), 0xff, 0xff])
    return ((read[1] << 8) | read[2])
def write_16bit_reg(reg, val):
    spi.xfer2([comm_reg(0,reg), (val >> 8) & 0xff, (val & 0xff)])
def read_24bit_reg(reg):
    read = spi.xfer2([comm_reg(1,reg), 0xff, 0xff, 0xff])
    return ((read[1] << 16) | read[2] << 8 | read[3])
def write_24bit_reg(reg, val):
    spi.xfer2([comm_reg(0,reg), (val >> 16) & 0xff, (val >> 8) & 0xff, (val & 0xff)])



def check_id():
    read = read_reg(REG_ID)
    if ((read & 0xf) != 0xB):
        return False
    return True

def reset_device():
    spi.xfer2([0xff,0xff,0xff,0xff])
    
def set_mode(mode):
    v = UPDATE_RATE | (CLOCK_REF << 6) | (mode << 13)
    write_16bit_reg(REG_MODE_16, UPDATE_RATE | (CLOCK_REF << 6) | (mode << 13))
    if (read_16bit_reg(REG_MODE_16) != v):
        return False
    return True

def set_conf(ch):
    v = ch | (BUFFER_INPUT << 4) | (REF_SEL << 7) | (GAIN << 8) | (BOOST << 11) | (POLARITY << 12) | (BIAS << 14)
    write_16bit_reg(REG_CONF_16, v) 
    if (read_16bit_reg(REG_CONF_16) != v):
        return False
    return True

def set_io():
    v = (IEXCDIR << 2) | (IEXCEN << 0)
    write_reg(REG_IO, (IEXCDIR << 2) | (IEXCEN << 0))
    if (read_reg(REG_IO) != v):
        return False
    return True
    

    
spi = spidev.SpiDev()
spi.open(0,0)
spi.lsbfirst = False
spi.mode = 3
spi.max_speed_hz = 100000

reset_device()
if (not check_id()):
    print "Device not found"
    sys.exit(1)

if (not set_io()):
    print "Error setting IO_REG"
    sys.exit(1)
    
if (not set_conf(0)):
    print "Error setting CONF_REG"
    sys.exit(1)
    
if (not set_mode(MODE_IDLE)):
    print "Error setting CONF_MODE"
    sys.exit(1)

time.sleep(0.15)

set_mode(MODE_INTERNAL_ZERO)
while (True):
    if status_is_ready(read_reg(REG_STATUS_R)):
        break    

set_conf(2)
set_mode(MODE_INTERNAL_FULL)
while (True):
    if status_is_ready(read_reg(REG_STATUS_R)):
        break

v = read_24bit_reg(REG_FULL_24)
set_conf(0)
write_24bit_reg(REG_FULL_24, v)

set_mode(MODE_SINGLE)

while (True):
    if status_is_ready(read_reg(REG_STATUS_R)):
        break
v = read_24bit_reg(REG_DATA_24)
uv = int(((v - 0x800000) / ((0x1000000-1)*1.0)) * 2100000.0)
print str(uv) + " uV "
print str(round((7000-(((uv - phF2) / phF1(temp)))*phF3)/1000,3)) + " pH"

sys.exit(0)

